'use strict';

// shader.js
// the programmable part of the rendering pipeline
// include vertex shader, pixel shader and direction/point light shader

function ShaderDevice(device, camera) {
    this.device = device;
    this.camera = camera;
}

// vertex shader (with direction light)
// perform matrix transformation on the vertexs
ShaderDevice.prototype.DirectionLightShader_VS = function (vsInput, worldMatrix, viewMatrix, projectionMatrix) {

    // matrix transformation
    let transformMatrix = worldMatrix.multiply(viewMatrix).multiply(projectionMatrix);

    // compute the 2d position
    let position2D = Vector3.TransformCoordinates(vsInput.position, transformMatrix);

    let normal = Vector3.TransformNormal(vsInput.normal, worldMatrix);

    return ({
        position: position2D,
        normal: normal,
        texcoord: vsInput.texcoord
    });
};

/**
 * pixel shader (with direction light)
 * @param {object} psInput the rasterized result (fragment) to be processed
 * @param {Texture} texture texture to be processed by pixel shader (to add to pixels)
 * @param {Light} light light 
 */
// 片元由片元着色器处理，代表可以在屏幕上绘制的像素。
ShaderDevice.prototype.DirectionLightShader_PS = function (psInput, texture, light) {

    let normal = psInput.normal;
    let lightf = light.directionLight.direction.negate();

    normal.normalize();
    lightf.normalize();

    let nd = Math.min(Math.max(0, Vector3.Dot(normal, lightf)), 1);

    let textureColor;

    // judge if texture is available
    if (texture) {
        textureColor = texture.TextureMap(psInput.texcoord.x, psInput.texcoord.y);
    } else {
        textureColor = new Color4(1, 1, 1, 1);
    }

    let ambient = 0.1;

    textureColor = textureColor.multiply(nd + ambient);

    return textureColor;
};

// vertex shader (with point light)
ShaderDevice.prototype.PointLightShader_VS = function (vsInput, worldMatrix, viewMatrix, projectionMatrix) {

    let transformMatrix = worldMatrix.multiply(viewMatrix).multiply(projectionMatrix);

    // position 2d -- transformed with world, view and projection matrix
    let position2D = Vector3.TransformCoordinates(vsInput.position, transformMatrix);

    // coordinate position in the world matrix
    let worldPos = Vector3.TransformCoordinates(vsInput.position, worldMatrix);
    let normal = Vector3.TransformNormal(vsInput.normal, worldMatrix);

    return ({
        worldPosition: worldPos,
        position: position2D,
        normal: normal,
        texcoord: vsInput.texcoord
    });
};

// pixel shader (with point light)
ShaderDevice.prototype.PointLightShader_PS = function (psInput, texture, light) {

    let normal = psInput.normal;
    let lightp = light.pointLight.position;

    let lightd = lightp.subtract(psInput.worldPosition);

    normal.normalize();
    lightd.normalize();

    let nd = Math.max(0, Vector3.Dot(normal, lightd));

    let textureColor;

    if (texture) {
        textureColor = texture.TextureMap(psInput.texcoord.x, psInput.texcoord.y);
    } else {
        textureColor = new Color4(1, 1, 1, 1);
    }

    let ambient = 0.1;

    textureColor = textureColor.multiply(nd + ambient);

    return textureColor;
};
