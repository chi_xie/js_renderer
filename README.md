# Web Renderer

A software rasterizer based on Javascript and HTML canvas.

Project Team:

| Name       | Student ID |
| ---------- | ---------- |
| Chi XIE    | 1552636    |
| Mingjie Ma | 1452735    |

  

## Overview

This is to accomplish the final project of Computer Graphics course Spring 2018, SSE, Tongji Univ., tutored by Prof. Jia. We devised a web-based renderer for rasterization of 3d models.

### Finished Job

- Primary Rendering
  - Transformation
  - Clipping
  - Rasterization
    - Interpolation
    - Bresenham Line-drawing
    - Scanline Algorithm
    - Z-Buffer
- Secondary Rendering
  - Wireframe Rendering (线框渲染)
  - Texture Rendering (纹理渲染)
- Lighting 
  - Direction Light (平行光源)
  - Point Light (点光源)
- Culling
  - Backface Culling (背面剔除)



## Analysis & Design

### Javascript & Web

Shown below is a result of searching the keyword “renderer” on Github. 

![search_github](./doc_imgs/search_github.png)

Clearly can be seen the fact that JavaScript implementations (including HTML) exceed other options in number by a large margin.

We choose to use Javascript to implement the software renderer for following reasons:

- Personal interest on web 3d. 
- Rich Resources for us to study.
- The innate nature of Web being cross-platform and environment-independent makes it easy for us to develop, debug and demonstrate our result.
- Members of this team use different operating systems, which means language like C++ or C# may bring some trouble for our teamwork.

We need to render the result image with a html canvas element. No WebGL is used here. 



### Rasterization

Rasterization means taking an image described in a vector graphics format (shapes) and converting it into a raster image (pixels or dots).  It refers to both rasterization of models and 2D rendering primitives such as point, line and triangle. In this part, rasterization refers to the latter meaning. 

The most basic rasterization algorithm takes a 3D scene, described as polygons, and renders it onto a 2D surface. Polygons are themselves represented as collections of triangles. Triangles are represented by 3 vertices in 3D-space. At a very basic level, rasterizers simply take a stream of vertices, transform them into corresponding 2-dimensional points on the viewer’s monitor and fill in the transformed 2-dimensional triangles as appropriate.

#### Bresenham

Bresenham's line algorithm is an algorithm that determines the points of a raster that should be selected in order to form a close approximation to a straight line between two points. 

![bresenham](./doc_imgs/bresenham.gif)

Here is a psudocode algorithm for Bresenham:

```pseudocode
e = 0, y = y1
for x = x1 to x2
	plot point at (x, y)
	if (e + m) > -0.5
		e = (e + m)
	else
		y = y - 1, e = e + m + 1
	end if 
end for
```

#### Scanline

There are a number of algorithms to fill in pixels inside a triangle, the most popular of which is the scanline algorithm.

- Walk along edges one scanline at a time
- Rasterize spans between edges

![scanline](./doc_imgs/scanline.png)

#### Z-Buffering

Since it is difficult to know that the rasterization engine will draw all pixels from front to back, there must be some way of ensuring that pixels close to the viewer are not overwritten by pixels far away. A z-buffer is the most common solution. The z buffer is a 2d array corresponding to the image plane which stores a depth value for each pixel. Whenever a pixel is drawn, it updates the z buffer with its depth value. Any new pixel must check its depth value against the z buffer value before it is drawn. Closer pixels are drawn and farther pixels are disregarded.

- When rendering multiple triangles we need to determine which triangles are visible
- Use z-buffer to resolve visibility
  - stores the depth `at each pixel
- Initialize z-buffer to 1
  - Post-perspective z values lie between 0 and 1
- Linearly interpolate depth ($Z_{tri}$) across triangles
- If $z_{tri}(x,y) < zBuffer[x][y]$, write to pixel at $(x,y)$,  $zBuffer[x][y] = z_{tri}(x,y)$



### Math

#### Coordinate System

![coordinate](./doc_imgs/coordinate.png)

#### Transformation

Transformations are usually performed by matrix multiplication. Quaternion math may also be used but that is outside the scope of this article. The main transformations are translation, scaling, rotation, and projection. A three-dimensional vertex may be transformed by augmenting an extra variable (known as a "homogeneous variable") and left multiplying the resulting 4-component vertex by a 4×4 transformation matrix.

A **translation** is simply the movement of a point from its original location to another location in 3-space by a constant offset. Translations can be represented by the following matrix.

A **scaling** transformation is performed by multiplying the position of a vertex by a scalar value. This has the effect of scaling a vertex with respect to the origin. Scaling can be represented by the following matrix.

**Rotation** matrices depend on the axis around which a point is to be rotated.



### Shader

![shader](./doc_imgs/shader.png)

Shaders are small programs in the graphics pipeline that is programmable, with input and output. Here we focus on vertex shader and pixel shader. Vertex and Pixel shaders provide different functions within the graphics pipeline. 

#### Vertex Shader

Vertex shaders take and process vertex-related data (positions, normals, texcoords). It binds data with the 3d points of models and produce primitives. 

#### Pixel Shader

Pixel (or more accurately, Fragment) shaders take values interpolated from those processed in the vertex shader and generate pixel fragments. Most of the "cool" stuff is done in pixel shaders. This is where things like texture lookup and lighting take place. To be simple, it adds color (or texture) to pixels (fragments).



### Texture Mapping

#### Texture Maps in OpenGL

- Specify normalized texture coordinates at each of the vertices (u, v)
-  Texel indices (s,t) = (u, v) ⋅ (width, height)

![texture_ogl](./doc_imgs/texture_ogl.png)

#### Linear interpolation of texture coordinates

Simple linear interpolation of u and v over a triangle leads to unexpected results. Distortion when the triangle vertices do not have the same depth.

![texture_simple](./doc_imgs/texture_simple.png)

Uniform steps along the edge projection in screen space do not correspond to uniform steps along the actual edge in eye space.

![texture_space](./doc_imgs/texture_space.png)

For screen space:
$$
p(\tau_s) = p_1 + \tau_s(p_2 - p_1) = \frac{x_1}{z_1} + \tau_s(\frac{x_2}{z_2} - \frac{x_1}{z_1})
$$
For eye space:
$$
v(\tau_e) = v_1 + \tau_e (v_2 - v_1)
$$

$$
p(v(\tau_e)) = \frac{x(\tau_e)}{z(\tau_e)} = \frac {x_1 + \tau_e(x_2-x_1)}{z_1+\tau_e(z_2-z_1)}
$$

We want to interpolate in eye space, but in terms of our screen space $\tau_s$. So we solve $p(\tau_s) = p(v(\tau_e))$ for $\tau_e$ in terms of $\tau_s$: 
$$
p(\tau_s) = \frac{x_1}{z_1} + \tau_s(\frac{x_2}{z_2} - \frac{x_1}{z_1}) = \frac{x_1+\tau_e(x_2-x_1)}{z_1+\tau_e(z_2-z_1)}=p(v(\tau_e))
$$

$$
\tau_e = \frac{\tau_s z_1} {z_2+\tau_s(z_1 - z_2)}
$$

Now we plug this value of $\tau_e$ into the equation to used to linearly interpolate parameters like (u, v) over the triangle in eye space:
$$
u(\tau_e) = u_1 + \tau_e(u_2-u_1)
$$

$$
u(\tau_s) = \frac{ u_1/w_1 + \tau_s (u_2/w_2 - u_1 / w_1) } {1/w_1 + \tau_s (1/w_1 - 1/w_2)}
$$

Linearly interpolate the numerator and denominator separately and do the divide once per pixel. 

-  Numerator is the linear interpolation of a parameter pre-divided by its corresponding w value
- Denominator is the linearly interpolated 1/w value

In clip coordinates, a triangle is still planar so a parameter can be interpolated over its surface as: 
$$
\frac{u}{1} = \frac{A_u x + B_u y + C_u w} {A_0x +B_0 y + C_0 w}
$$
Division by w produces the **perspective-correct interpolation** equation in screen space:
$$
\frac {u/w} { 1/w} = \frac{A_u x/w + B_u y /w + C_u} {A_0 x/w + B_0 y/w + C_0} = \frac{A_ux + B_u y + C_u} { A_0 x + B_0 y + C_0}
$$
![texture_interpolation](./doc_imgs/texture_interpolation.png)



### Backface Culling

The simplest way to cull polygons is to cull all polygons which face away from the viewer. This is known as backface culling. 

Since most 3d objects are fully enclosed, polygons facing away from a viewer are always blocked by polygons facing towards the viewer unless the viewer is inside the object. A polygon’s facing is defined by its winding, or the order in which its vertices are sent to the renderer. A renderer can define either **clockwise(CW)** or **counterclockwise(CCW)** winding as front or back facing. 

Once a polygon has been transformed to screen space, its winding can be checked and if it is in the opposite direction, it is not drawn at all. Of course, backface culling can not be used with degenerate and unclosed volumes.



### Lighting

Before the final color of the pixel can be decided, a lighting calculation must be performed to shade the pixels based on any lights which may be present in the scene. There are generally three light types commonly used in scenes: directional light, point light and spotlight. Here we choose the former two kinds of light. 

#### Directional Light

Directional lights are lights which come from a single direction and have the same intensity throughout the entire scene. In real life, sunlight comes close to being a directional light, as the sun is so far away that rays from the sun appear parallel to Earth observers and the falloff is negligible. 

#### Point Light

Point lights are lights with a definite position in space and radiate light evenly in all directions. Point lights are usually subject to some form of attenuation, or fall off in the intensity of light incident on objects farther away. Real life light sources experience quadratic falloff. 



## Implementation

### Project Structure

![structure](./doc_imgs/structure.png)

### Teamwork

We use git and bitbucket for version control and team cooperation. The code has also been uploaded to bitbucket repo: https://bitbucket.org/chi_xie/js_renderer/



### Functions

#### Shader

##### Vertex Shader (Directional Light)

```javascript
// vertex shader (with direction light)
// perform matrix transformation on the vertexs
ShaderDevice.prototype.DirectionLightShader_VS = function (vsInput, worldMatrix, viewMatrix, projectionMatrix) {

    // matrix transformation
    let transformMatrix = worldMatrix.multiply(viewMatrix).multiply(projectionMatrix);

    // compute the 2d position
    let position2D = Vector3.TransformCoordinates(vsInput.position, transformMatrix);

    let normal = Vector3.TransformNormal(vsInput.normal, worldMatrix);

    return ({
        position: position2D,
        normal: normal,
        texcoord: vsInput.texcoord
    });
};
```

##### Vertex Shader (Point Light)

```javascript
// vertex shader (with point light)
ShaderDevice.prototype.PointLightShader_VS = function (vsInput, worldMatrix, viewMatrix, projectionMatrix) {

    let transformMatrix = worldMatrix.multiply(viewMatrix).multiply(projectionMatrix);

    // position 2d -- transformed with world, view and projection matrix
    let position2D = Vector3.TransformCoordinates(vsInput.position, transformMatrix);

    // coordinate position in the world matrix
    let worldPos = Vector3.TransformCoordinates(vsInput.position, worldMatrix);
    let normal = Vector3.TransformNormal(vsInput.normal, worldMatrix);

    return ({
        worldPosition: worldPos,
        position: position2D,
        normal: normal,
        texcoord: vsInput.texcoord
    });
};
```

##### Pixel Shader (Directional Light)

```javascript
/**
 * pixel shader (with direction light)
 * @param {object} psInput the rasterized result (fragment) to be processed
 * @param {Texture} texture texture to be processed by pixel shader (to add to pixels)
 * @param {Light} light light 
 */
// 片元由片元着色器处理，代表可以在屏幕上绘制的像素。
ShaderDevice.prototype.DirectionLightShader_PS = function (psInput, texture, light) {

    let normal = psInput.normal;
    let lightf = light.directionLight.direction.negate();

    normal.normalize();
    lightf.normalize();

    let nd = Math.min(Math.max(0, Vector3.Dot(normal, lightf)), 1);

    let textureColor;

    // judge if texture is available
    if (texture) {
        textureColor = texture.TextureMap(psInput.texcoord.x, psInput.texcoord.y);
    } else {
        textureColor = new Color4(1, 1, 1, 1);
    }

    let ambient = 0.1;

    textureColor = textureColor.multiply(nd + ambient);

    return textureColor;
};
```

##### Pixel Shader (Point Light)

```javascript
// pixel shader (with point light)
ShaderDevice.prototype.PointLightShader_PS = function (psInput, texture, light) {

    let normal = psInput.normal;
    let lightp = light.pointLight.position;

    let lightd = lightp.subtract(psInput.worldPosition);

    normal.normalize();
    lightd.normalize();

    let nd = Math.max(0, Vector3.Dot(normal, lightd));

    let textureColor;

    if (texture) {
        textureColor = texture.TextureMap(psInput.texcoord.x, psInput.texcoord.y);
    } else {
        textureColor = new Color4(1, 1, 1, 1);
    }

    let ambient = 0.1;

    textureColor = textureColor.multiply(nd + ambient);

    return textureColor;
};
```

#### Rasterization

##### Scan-line Algorithm

```javascript
// scanline algorithm
Raster.prototype.processScanLine = function (y, va, vb, vc, vd, res) {

    let pa = va.position;
    let pb = vb.position;
    let pc = vc.position;
    let pd = vd.position;

    let gradient1 = (pa.y !== pb.y) ? (y - pa.y) / (pb.y - pa.y) : 1;
    let gradient2 = (pc.y !== pd.y) ? (y - pc.y) / (pd.y - pc.y) : 1;

    let sv = this.interpolateVertex(va, vb, gradient1);
    let ev = this.interpolateVertex(vc, vd, gradient2);

    // set sx < ex
    if (sv.position.x > ev.position.x) {
        let tmp;
        tmp = sv;
        sv = ev;
        ev = tmp;
    }

    for (let x = sv.position.x; x < ev.position.x; x++) {
        let gradient = (x - sv.position.x) / (ev.position.x - sv.position.x);
        let v = this.interpolateVertex(sv, ev, gradient);
        res.push(v);
    }
};
```

##### Triangle Rasterization

```javascript
// rasterization performed on triangle
Raster.prototype.RasterTriangle = function (v1, v2, v3) {

    let y;
    let temp;
    if (v1.position.y > v2.position.y) {
        temp = v2;
        v2 = v1;
        v1 = temp;
    }
    if (v2.position.y > v3.position.y) {
        temp = v2;
        v2 = v3;
        v3 = temp;
    }
    if (v1.position.y > v2.position.y) {
        temp = v2;
        v2 = v1;
        v1 = temp;
    }

    // 反向斜率
    let dP1P2;
    let dP1P3;

    if (v2.position.y - v1.position.y > 0) {
        dP1P2 = (v2.position.x - v1.position.x) / (v2.position.y - v1.position.y);
    } else {
        dP1P2 = 0;
    }

    if (v3.position.y - v1.position.y > 0) {
        dP1P3 = (v3.position.x - v1.position.x) / (v3.position.y - v1.position.y);
    } else {
        dP1P3 = 0;
    }

    let res = [];

    if (dP1P2 > dP1P3) {
        for (y = v1.position.y >> 0; y <= v3.position.y >> 0; y++) {
            if (y < v2.position.y) {
                this.processScanLine(y, v1, v3, v1, v2, res);
            } else {
                this.processScanLine(y, v1, v3, v2, v3, res);
            }

        }

    } else {
        for (y = v1.position.y >> 0; y <= v3.position.y >> 0; y++) {
            if (y < v2.position.y) {
                this.processScanLine(y, v1, v2, v1, v3, res);
            } else {
                this.processScanLine(y, v2, v3, v1, v3, res);
            }
        }
    }

    return res;
};
```

##### Bresenham Line-Drawing

```javascript
// Bresenham line-drawing
Raster.prototype.drawLine = function (v0, v1) {
    let x0 = v0.position.x >> 0;
    let y0 = v0.position.y >> 0;
    let x1 = v1.position.x >> 0;
    let y1 = v1.position.y >> 0;
    let dx = Math.abs(x1 - x0);
    let dy = Math.abs(y1 - y0);
    let sx = (x0 < x1) ? 1 : -1;
    let sy = (y0 < y1) ? 1 : -1;
    let err = dx - dy;
    while (true) {
        this.device.drawPoint(new Vector2(x0, y0, 1.0), new Color4(1, 1, 1, 1));
        if ((x0 === x1) && (y0 === y1)) break;
        let e2 = 2 * err;
        if (e2 > -dy) {
            err -= dy;
            x0 += sx;
        }
        if (e2 < dx) {
            err += dx;
            y0 += sy;
        }
    }
};
```

#### Back-face Culling

##### CCW Culling Judging

```shell
// CCW culling
// CW and CCW mean clockwise or counter-clockwise is front, respectively
// judge whether or not need to be culled
Raster.prototype.ccwJudge = function (v1, v2, v3) {
    // TODO
    let d1 = v2.position.subtract(v1.position);
    let d2 = v3.position.subtract(v1.position);
    d1.z = 0;
    d2.z = 0;

    let d = Vector3.Cross(d1, d2);

    d.normalize();

    let lhr = new Vector3(0, 0, 1);

    return Vector3.Dot(d, lhr) < 0;
};
```

#### Texture Mapping

##### Wrap Texture

```javascript
// texture mapping - Wrap method
Texture.prototype.TextureMap = function (tu, tv) {

    if (this.internalBuffer) {
        let u = Math.abs(((tu * this.width) % this.width)) >> 0;
        let v = Math.abs(((tv * this.height) % this.height)) >> 0;

        let pos = (u + v * this.width) * 4;

        let r = this.internalBuffer.data[pos];
        let g = this.internalBuffer.data[pos + 1];
        let b = this.internalBuffer.data[pos + 2];
        let a = this.internalBuffer.data[pos + 3];

        return new Color4(r / 255.0, g / 255.0, b / 255.0, a / 255.0);
    } else {
        // no texture available
        return new Color4(1, 1, 1, 1);
    }
};
```



## Result

### How to Run

1. install node.js
2. run the command: `npm install -g serve`
3. enter the root command of this project
4. run: `serve`
5. open the browser and enter the address : localhost:5000, and you should see the renderer page. 



### Performance

The demonstration page shows when you enter the address in your browser:

![main](./doc_imgs/main.png)

Options in detail:

![options](./doc_imgs/options.png)

#### Texture Rendering

Just click 'start rendering'. This is the texture mode with directional light.

![render](./doc_imgs/render.png)

FPS is approximately 35.

#### Wireframe Rendering

![wireframe](./doc_imgs/wireframe.png)

FPS is approxmately 59.

#### Depth Test

FPS is approximately 29.

![depth_test](./doc_imgs/depth_test.png)

#### Backface Culling

Without CCW and CW, the FPS is approximately 18.

WIth one of CCW and CW culling, the FPS is approximately 30.

#### Light

You can switch to point light source by just clicking the checkbox.

![point](./doc_imgs/point.png)

#### Position Adjustment

You can also change the position of light source or camera on the control pane.

When the camera is closer and the cubes zoom in, the FPS decrease a lot. 



## Review

Such a web renderer has been created as expected.We would like to add more features to it in the near future. See TODO log on bitbucket repository. This project gives us a comprehensive understanding about computer graphics and motivates us to further develop our skills. It also showed us how interesting such a discipline can become when one tries hard to turn the knowledge he/she has learnt from textbooks into amazing things.



## Reference 

[1] COMP 770 Computer Graphics Lecture Slides on Culling, 3D Transformations, Texture Mapping and Triangle Rasterization: http://www.cs.unc.edu/~blloyd/comp770/

[2] Wikipedia contributors. (2018, June 17). Rasterisation. In *Wikipedia, The Free Encyclopedia*. Retrieved 16:02, July 8, 2018, from https://en.wikipedia.org/w/index.php?title=Rasterisation&oldid=846273695

[3] Wikipedia contributors. (2018, April 4). Graphics pipeline. In *Wikipedia, The Free Encyclopedia*. Retrieved 16:05, July 8, 2018, from https://en.wikipedia.org/w/index.php?title=Graphics_pipeline&oldid=834173660

[4] Wikipedia contributors. (2018, June 14). Shader. In *Wikipedia, The Free Encyclopedia*. Retrieved 16:13, July 8, 2018, from https://en.wikipedia.org/w/index.php?title=Shader&oldid=845848352

[5] Mini3d - 3D Software Render Engine in 700 Lines. by skywind3000. https://github.com/skywind3000/mini3d

[6] How to Write a Simple Software Rasterizer. from cnblogs. http://www.cnblogs.com/hust-ruan/archive/2013/03/16/2962077.html

[7] Implementing a Simple Software Rasterizer Using C#. from CSDN. https://blog.csdn.net/aceyan0718/article/details/51659381

[8] How to Start Writing a Software Rasterizer Usign C++? Answers from zhihu. https://www.zhihu.com/question/24786878